package com.example.rkpt.ui.people;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rkpt.R;
import com.example.rkpt.ui.AddFriendActivity;
import com.example.rkpt.ui.ChatActivity;
import com.example.rkpt.ui.FragmentStatusSender;
import com.example.rkpt.ui.FriendsListAdapter;
import com.example.rkpt.ui.HomePage;
import com.example.rkpt.ui.RecyclerItemClickListener;
import com.example.rkpt.ui.User;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Collections;

public class PeopleFragment extends Fragment implements FriendsListAdapter.OnItemListener {
    public final ArrayList<String> friendsUsername = new ArrayList<>();
    public String username_to_chat_with="";

    public String getUsername_to_chat_with() {
        return username_to_chat_with;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final FragmentStatusSender fragmentStatusSender = new FragmentStatusSender("fragment");
        fragmentStatusSender.setFragment("friend");
        fragmentStatusSender.start();
        try {
            fragmentStatusSender.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String serverDetails = fragmentStatusSender.getFromServer();
        String friends = serverDetails.substring(serverDetails.indexOf("$") + 1);
        final String[] friendsToArray = friends.split(" ");
        for (int i = 0; i < friendsToArray.length; i++) {
            if (!friendsToArray[i].equals("")) {
                friendsUsername.add(friendsToArray[i]);
            }
        }
        View root = inflater.inflate(R.layout.fragment_people, container, false);
        FloatingActionButton addFriend = root.findViewById(R.id.add_friend);
        final Intent intent = new Intent(getContext(), AddFriendActivity.class);
        addFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(intent);
            }
        });
        if (!friendsUsername.isEmpty()) {
            final RecyclerView friendsList = root.findViewById(R.id.friends_list);
            friendsList.setLayoutManager(new LinearLayoutManager(getActivity()));
            FriendsListAdapter friendsListAdapter = new FriendsListAdapter(friendsUsername, this);
            friendsList.setAdapter(friendsListAdapter);
            friendsList.setItemAnimator(new DefaultItemAnimator());

        }
        return root;
    }

    @Override
    public void onItemClick(int position) {
        username_to_chat_with = friendsUsername.get(position);
        Intent intent_new = new Intent(getContext(), ChatActivity.class);
        intent_new.putExtra("userToChat",username_to_chat_with);
        startActivity(intent_new);
    }

}