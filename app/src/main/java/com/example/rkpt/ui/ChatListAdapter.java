package com.example.rkpt.ui;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rkpt.R;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;


public class ChatListAdapter extends RecyclerView.Adapter {
    private List<Message> list;
    ChatListAdapter(List<Message> list){
        this.list = list;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message, parent, false);
        return new MessageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        MessageViewHolder mvh = (MessageViewHolder)holder;
        mvh.bind(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private class MessageViewHolder extends RecyclerView.ViewHolder{
        TextView messageText, timeText, nameText;

        MessageViewHolder(View itemView){
            super(itemView);
            messageText = itemView.findViewById(R.id.message);
            timeText= itemView.findViewById(R.id.date);
            nameText = itemView.findViewById(R.id.username_in_chat);
        }

        void bind(Message message) {
            messageText.setText(message.getBody());
            timeText.setText(message.getDate());
            nameText.setText(message.getSender());
        }

    }
}