package com.example.rkpt.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.rkpt.R;
import com.example.rkpt.ui.HomePage;
import com.example.rkpt.ui.NetworkHandlerThread;
import com.example.rkpt.ui.User;
import com.example.rkpt.ui.Users;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class Login extends AppCompatActivity {
    static Socket socket;
    static DataOutputStream dos;
    static DataInputStream dis;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Intent intent = new Intent(this, HomePage.class);
//        startActivity(intent);
        final TextView errorFromServer = findViewById(R.id.errorView);
        Button sign_in = findViewById(R.id.signin);
        Button sign_up = findViewById(R.id.signup);
        sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String enteredUsername;
                String enteredPassword;
                EditText username = (EditText) findViewById(R.id.username);
                EditText password = (EditText) findViewById(R.id.password);
                enteredUsername = username.getText().toString();
                enteredPassword = password.getText().toString();
                String to_send = "signin$"+enteredUsername+" "+enteredPassword;
                NetworkHandlerThread networkHandlerThread = new NetworkHandlerThread(to_send,errorFromServer);
                networkHandlerThread.start();
                try {
                    networkHandlerThread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (networkHandlerThread.getPassLogin()){
                    startActivity(intent);
                }
            }
        });
        sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String enteredUsername;
                String enteredPassword;
                EditText username = (EditText) findViewById(R.id.username);
                EditText password = (EditText) findViewById(R.id.password);
                enteredUsername = username.getText().toString();
                enteredPassword = password.getText().toString();
                String to_send = "signup$"+enteredUsername+" "+enteredPassword;
                NetworkHandlerThread networkHandlerThread = new NetworkHandlerThread(to_send,errorFromServer);
                networkHandlerThread.start();
                try {
                    networkHandlerThread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (networkHandlerThread.getPassLogin()){
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        setContentView(R.layout.activity_main);
        final Intent intent = new Intent(this, HomePage.class);
//        startActivity(intent);
        final TextView errorFromServer = findViewById(R.id.errorView);
        Button sign_in = findViewById(R.id.signin);
        Button sign_up = findViewById(R.id.signup);
        sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String enteredUsername;
                String enteredPassword;
                EditText username = (EditText) findViewById(R.id.username);
                EditText password = (EditText) findViewById(R.id.password);
                enteredUsername = username.getText().toString();
                enteredPassword = password.getText().toString();
                String to_send = "signin$"+enteredUsername+" "+enteredPassword;
                NetworkHandlerThread networkHandlerThread = new NetworkHandlerThread(to_send,errorFromServer);
                networkHandlerThread.start();
                try {
                    networkHandlerThread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (networkHandlerThread.getPassLogin()){
                    startActivity(intent);
                }
            }
        });
        sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String enteredUsername;
                String enteredPassword;
                EditText username = (EditText) findViewById(R.id.username);
                EditText password = (EditText) findViewById(R.id.password);
                enteredUsername = username.getText().toString();
                enteredPassword = password.getText().toString();
                String to_send = "signup$"+enteredUsername+" "+enteredPassword;
                NetworkHandlerThread networkHandlerThread = new NetworkHandlerThread(to_send,errorFromServer);
                networkHandlerThread.start();
                try {
                    networkHandlerThread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (networkHandlerThread.getPassLogin()){
                    startActivity(intent);
                }
            }
        });

    }
}