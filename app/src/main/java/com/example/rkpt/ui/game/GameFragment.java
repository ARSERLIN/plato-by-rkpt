package com.example.rkpt.ui.game;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.rkpt.R;
import com.example.rkpt.ui.FragmentStatusSender;

public class GameFragment extends Fragment {
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final FragmentStatusSender fragmentStatusSender = new FragmentStatusSender("fragment");
        fragmentStatusSender.setFragment("game");
        fragmentStatusSender.start();
        try {
            fragmentStatusSender.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        View root = inflater.inflate(R.layout.fragment_game,container,false);
        return root;
    }
}
