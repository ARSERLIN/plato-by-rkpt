package com.example.rkpt.ui.chat;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rkpt.R;
import com.example.rkpt.ui.FragmentStatusSender;
import com.example.rkpt.ui.FriendsListAdapter;
import com.example.rkpt.ui.RecyclerItemClickListener;

import java.util.ArrayList;

public class ChatFragment extends Fragment implements FriendsListAdapter.OnItemListener {
        public final ArrayList<String> chatsList = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final FragmentStatusSender fragmentStatusSender = new FragmentStatusSender("fragment");
        fragmentStatusSender.setFragment("chat");
        fragmentStatusSender.start();
        try {
            fragmentStatusSender.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String serverDetails = fragmentStatusSender.getFromServer();
        String chats = serverDetails.substring(serverDetails.indexOf("$") + 1);
        final String[] chatsArray = chats.split(" ");
        for (int i = 0; i < chatsArray.length; i++) {
            if (!chatsArray[i].equals("")) {
                chatsList.add(chatsArray[i]);
            }
        }
        View root = inflater.inflate(R.layout.fragment_chat,container,false);
        if (!chatsList.isEmpty()) {
            final RecyclerView chatList = root.findViewById(R.id.chat_list);
            chatList.setLayoutManager(new LinearLayoutManager(getActivity()));
            FriendsListAdapter friendsListAdapter = new FriendsListAdapter(chatsList,this);
            chatList.setAdapter(friendsListAdapter);
            chatList.setItemAnimator(new DefaultItemAnimator());
        }
        return root;
    }

    @Override
    public void onItemClick(int position) {
        String username = chatsList.get(position);
        Log.i("DARD", username);

    }
}
