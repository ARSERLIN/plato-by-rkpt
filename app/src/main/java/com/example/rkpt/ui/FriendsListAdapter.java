package com.example.rkpt.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rkpt.R;

import java.util.ArrayList;

public class FriendsListAdapter extends RecyclerView.Adapter<FriendsListAdapter.ViewHolder> {
    private ArrayList<String> friendsList;
    private OnItemListener onItemListener;

    public FriendsListAdapter(ArrayList<String> friendsList,OnItemListener onItemListener) {
        this.friendsList = friendsList;
        this.onItemListener = onItemListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_friend, parent, false);
        return new ViewHolder(view,onItemListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ViewHolder vh = (ViewHolder) holder;
        vh.bind(this.friendsList.get(position));
    }

    @Override
    public int getItemCount() {
        return this.friendsList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView friendUsername;
        OnItemListener onItemListener;
        ViewHolder(View itemView,OnItemListener onItemListener) {
            super(itemView);
            this.friendUsername = itemView.findViewById(R.id.friend_name);
            itemView.setOnClickListener(this);
            this.onItemListener = onItemListener;

        }

        @Override
        public void onClick(View view) {
            onItemListener.onItemClick(getAdapterPosition());
        }

        void bind(String username) {
            this.friendUsername.setText(username);
        }
    }
    public interface OnItemListener{
        void onItemClick(int position);
    }
}
