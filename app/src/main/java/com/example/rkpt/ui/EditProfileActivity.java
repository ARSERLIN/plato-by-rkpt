package com.example.rkpt.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.IntegerRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.rkpt.R;

public class EditProfileActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        final TextView messageFromServer = findViewById(R.id.edit_message_from_server);
        Button editUsername = findViewById(R.id.changeUsername_button);
        Button editPassword = findViewById(R.id.changePassword_button);
        Button editBio = findViewById(R.id.changeBio_button);
        Button save = findViewById(R.id.back_to_profile);
        editUsername.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final EditText newUsername = (EditText) findViewById(R.id.changeUsername);
                String enteredUsername = newUsername.getText().toString();
                ActivityStatusSender activityStatusSender = new ActivityStatusSender("editusername");
                activityStatusSender.setToChange(enteredUsername);
                activityStatusSender.setServerReactToChanges(messageFromServer);
                activityStatusSender.start();
                try {
                    activityStatusSender.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        editPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final EditText newPassword = (EditText) findViewById(R.id.changePassword);
                String enteredPassword = newPassword.getText().toString();
                ActivityStatusSender activityStatusSender = new ActivityStatusSender("editpassword");
                activityStatusSender.setToChange(enteredPassword);
                activityStatusSender.setServerReactToChanges(messageFromServer);
                activityStatusSender.start();
                try {
                    activityStatusSender.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        });
        editBio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final EditText newBio = (EditText) findViewById(R.id.changeBio);
                String enteredBio = newBio.getText().toString();
                ActivityStatusSender activityStatusSender = new ActivityStatusSender("editbio");
                activityStatusSender.setToChange(enteredBio);
                activityStatusSender.setServerReactToChanges(messageFromServer);
                activityStatusSender.start();
                try {
                    activityStatusSender.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        });
        final Intent intent = new Intent(this,ProfileActivity.class);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(intent);
            }
        });
    }

}
