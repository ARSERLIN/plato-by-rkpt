package com.example.rkpt.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.rkpt.R;
import com.example.rkpt.ui.chat.ChatFragment;
import com.example.rkpt.ui.game.GameFragment;
import com.example.rkpt.ui.home.HomeFragment;
import com.example.rkpt.ui.people.PeopleFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class HomePage extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);
        HomeFragment homeFragment = new HomeFragment();
        openFragment(homeFragment);
        BottomNavigationView bottomNavigationView = this.findViewById(R.id.bottom_navigation);
        BottomNavigationView upBar = this.findViewById(R.id.up_bar);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.home_nav:
                        HomeFragment homeFragment = new HomeFragment();
                        openFragment(homeFragment);
                        break;
                    case R.id.game_nav:
                        GameFragment gameFragment = new GameFragment();
                        openFragment(gameFragment);
                        break;
                    case R.id.chat_nav:
                        ChatFragment chatFragment = new ChatFragment();
                        openFragment(chatFragment);
                        break;
                    case R.id.friends_nav:
                        PeopleFragment peopleFragment = new PeopleFragment();
                        openFragment(peopleFragment);
                        break;
                }
                return true;
            }
        });
        final Intent toProfile = new Intent(this, ProfileActivity.class);
        upBar.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if (item.getItemId() == R.id.profile){
                    startActivity(toProfile);
                }
                return true;
            }
        });
    }
    public void openFragment(final Fragment fragment){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container,fragment);
//        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        setContentView(R.layout.home_activity);
        HomeFragment homeFragment = new HomeFragment();
        openFragment(homeFragment);
        BottomNavigationView bottomNavigationView = this.findViewById(R.id.bottom_navigation);
        BottomNavigationView upBar = this.findViewById(R.id.up_bar);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.home_nav:
                        HomeFragment homeFragment = new HomeFragment();
                        openFragment(homeFragment);
                        break;
                    case R.id.game_nav:
                        GameFragment gameFragment = new GameFragment();
                        openFragment(gameFragment);
                        break;
                    case R.id.chat_nav:
                        ChatFragment chatFragment = new ChatFragment();
                        openFragment(chatFragment);
                        break;
                    case R.id.friends_nav:
                        PeopleFragment peopleFragment = new PeopleFragment();
                        openFragment(peopleFragment);
                        break;
                }
                return true;
            }
        });
        final Intent toProfile = new Intent(this, ProfileActivity.class);
        upBar.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if (item.getItemId() == R.id.profile){
                    startActivity(toProfile);
                }
                return true;
            }
        });
    }
}
