package com.example.rkpt.ui;

import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;

import static com.example.rkpt.ui.Login.dis;
import static com.example.rkpt.ui.Login.dos;

public class ChatThread extends Thread {
    private String username;
    private String body;
    private String command;
    private ArrayList<Message> messages;
    //getChat
    ChatThread(String command,String username){
        this.command = command;
        this.username = username;
    }
    //send
    ChatThread(String command,String username,String body){
        this.command = command;
        this.username = username;
        this.body = body;
    }

    public ArrayList<Message> getMessages() {
        return messages;
    }

    @Override
    public void run() {
        switch (command){
            case "sendmessage":
                try {
                    dos.writeUTF("sendmessage$ "+username+" "+body);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            case "getchat":
                try {
                    dos.writeUTF("getchat$" + username);
                    String chats = dis.readUTF();
                    if (chats.startsWith("empty")) {

                    } else if (chats.startsWith("chat")) {
                        int messagesNumber = Integer.parseInt(chats.substring(chats.indexOf("$") + 1, chats.indexOf(" ")));
                        String justSndRcvDatAndMsg = chats.substring(chats.indexOf(" ") + 1);
                        String[] sndRcvDatAndMsg = justSndRcvDatAndMsg.split(" ");
                        String sender = "";
                        String receiver = "";
                        String date = "";
                        String body = "";
                        for (int i = 0; i < messagesNumber; i++) {
                            if (i % 4 == 0) {
                                sender = sndRcvDatAndMsg[i];
                            } else if (i % 4 == 1) {
                                receiver = sndRcvDatAndMsg[i];
                            } else if (i % 4 == 2) {
                                date = sndRcvDatAndMsg[i];
                            } else {
                                body = sndRcvDatAndMsg[i];
                                Message msg = new Message(date, receiver, sender, body);
                                messages.add(msg);
                            }
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
        }
    }
}
