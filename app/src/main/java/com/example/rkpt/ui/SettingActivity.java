package com.example.rkpt.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.rkpt.R;

import java.io.IOException;

import static com.example.rkpt.ui.Login.dis;
import static com.example.rkpt.ui.Login.dos;
import static com.example.rkpt.ui.Login.socket;

public class SettingActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_activity);
        Button aboutUs = findViewById(R.id.about_us_button);
        Button logout = findViewById(R.id.logout_button);
        final Intent toAboutUs = new Intent(this, AboutUsActivity.class);
        final Intent toLogin = new Intent(this, Login.class);
        aboutUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(toAboutUs);
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityStatusSender activityStatusSender = new ActivityStatusSender("logout");
                activityStatusSender.start();
                try {
                    activityStatusSender.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                try {
                    socket.close();
                    dis.close();
                    dos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                startActivity(toLogin);
            }
        });
    }
}
