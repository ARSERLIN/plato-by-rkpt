package com.example.rkpt.ui;

import android.content.Intent;
import android.util.Log;
import android.widget.TextView;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;

import static com.example.rkpt.ui.Login.dis;
import static com.example.rkpt.ui.Login.dos;

public class NetworkHandlerThread extends Thread {
    private String message;
    private TextView error;
    private boolean passLogin = false;

    public NetworkHandlerThread(String message, TextView error) {
        this.message = message;
        this.error = error;
    }

    public boolean getPassLogin() {
        return this.passLogin;
    }

    @Override
    public void run() {
        super.run();
        try {
            Login.socket = new Socket("192.168.1.2", 1380);
            dos = new DataOutputStream(Login.socket.getOutputStream());
            dis = new DataInputStream(Login.socket.getInputStream());
            dos.writeUTF(message);
            String fromServer = dis.readUTF();
            String tag = fromServer.substring(0, fromServer.indexOf('$'));
            if (tag.equals("success")) {
                this.passLogin = true;
            }
            tag = tag + "$";
            String errorMessage = fromServer.replace(tag, "");
            error.setText(errorMessage);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}
