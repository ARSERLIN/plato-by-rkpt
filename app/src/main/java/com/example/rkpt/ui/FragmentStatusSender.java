package com.example.rkpt.ui;

import android.widget.Switch;

import androidx.fragment.app.Fragment;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;

import static com.example.rkpt.ui.Login.dis;
import static com.example.rkpt.ui.Login.dos;
import static com.example.rkpt.ui.Login.socket;

public class FragmentStatusSender extends Thread {
    private String fragment;
    private String message;
    private String fromServer;
    private User userInfo;

    public FragmentStatusSender(String fragment) {
        this.fragment = fragment;
    }
    public void setFragment(String fragment){
        this.fragment = fragment;
    }

    public User getUserInfo() {
        return userInfo;
    }

    public String getFromServer() {
        return fromServer;
    }

    @Override
    public void run() {
        try {
            switch (fragment) {
                case "game":
                    message = "fragment$game";
                    dos.writeUTF(message);
                    fromServer = dis.readUTF();
                    break;
                case "chat":
                    message = "fragment$chat";
                    dos.writeUTF(message);
                    fromServer = dis.readUTF();
                    break;
                case "friend":
                    message = "fragment$friend";
                    dos.writeUTF(message);
                    fromServer = dis.readUTF();
                    break;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
