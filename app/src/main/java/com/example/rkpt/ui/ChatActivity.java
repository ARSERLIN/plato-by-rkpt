package com.example.rkpt.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rkpt.R;
import com.example.rkpt.ui.people.PeopleFragment;

import java.io.IOException;
import java.util.ArrayList;

import static com.example.rkpt.ui.Login.dis;
import static com.example.rkpt.ui.Login.dos;

public class ChatActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_activity);
        ArrayList<Message> messages = new ArrayList<>();
        Intent intent2 = getIntent();
        final String username = intent2.getStringExtra("userToChat");
        ChatThread chatThread = new ChatThread("getchat",username);
        chatThread.start();
        try {
            chatThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        messages.addAll(chatThread.getMessages());
        if (!messages.isEmpty()) {
            RecyclerView recyclerView = this.findViewById(R.id.messages_list);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            ChatListAdapter adapter = new ChatListAdapter(messages);
            recyclerView.setAdapter(adapter);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
        }
        Button toHome = findViewById(R.id.back_to_home_from_chat);
        Button refresh = findViewById(R.id.refresh);
        Button send = findViewById(R.id.send);
        final EditText typedMessage = (EditText)findViewById(R.id.type_message);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String typedMessageInBox = typedMessage.getText().toString();
                ChatThread chatThread = new ChatThread("sendmessage",username,typedMessageInBox);
                chatThread.start();
                try {
                    chatThread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        final Intent intent = new Intent(this, ChatActivity.class);
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(intent);
            }
        });
        final Intent homeStart = new Intent(this, HomePage.class);
        toHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(homeStart);
            }
        });
    }
}
