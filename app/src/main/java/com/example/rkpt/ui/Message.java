package com.example.rkpt.ui;

import java.io.Serializable;

public class Message implements Serializable {
    private String date;
    private String sender;
    private String reciever;
    private String body;

    public Message( String sender, String reciever, String body) {
        this.sender = sender;
        this.reciever = reciever;
        this.body = body;
    }
    public Message(String date , String sender, String reciever, String body) {
        this.date = date;
        this.sender = sender;
        this.reciever = reciever;
        this.body = body;
    }

    public String getReciever() {
        return reciever;
    }

    public void setReciever(String reciever) {
        this.reciever = reciever;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
