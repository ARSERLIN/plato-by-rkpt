package com.example.rkpt.ui;

import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rkpt.R;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;

import static com.example.rkpt.ui.Login.dis;
import static com.example.rkpt.ui.Login.dos;
import static com.example.rkpt.ui.Login.socket;

public class ActivityStatusSender extends Thread {
    private String activityName;
    private String message;
    private String fromServer;
    private String toChange;
    private TextView serverSaid;
    private TextView usernameToShow;
    private TextView bioToShow;
    private TextView serverReactToChanges;
    private boolean isLoggedIn = true;

    ActivityStatusSender(String activityName) {
        this.activityName = activityName;
    }

    public void setToChange(String toChange) {
        this.toChange = toChange;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public void setServerReactToChanges(TextView serverReactToChanges) {
        this.serverReactToChanges = serverReactToChanges;
    }

    public void setServerSaid(TextView serverSaid) {
        this.serverSaid = serverSaid;
    }

    public void setUsernameToShow(TextView usernameToShow) {
        this.usernameToShow = usernameToShow;
    }

    public void setBioToShow(TextView bioToShow) {
        this.bioToShow = bioToShow;
    }

    public TextView getServerSaid() {
        return serverSaid;
    }

    @Override
    public void run() {
        try {
            User user = new User("", "");
            switch (activityName) {
                case "profile":
                    message = "profile$";
                    dos.writeUTF(message);
                    fromServer = dis.readUTF();
                    user.setUsername(fromServer.substring(fromServer.indexOf("$") + 1, fromServer.indexOf(" ")));
                    user.setBio(fromServer.substring(fromServer.indexOf(" ") + 1));
                    break;
                case "logout":
                    message = "logout$";
                    dos.writeUTF(message);
                    isLoggedIn = false;
                    break;
                case "editusername":
                    message = "editprofile$username" + " " + toChange;
                    dos.writeUTF(message);
                    fromServer = dis.readUTF();
                    break;
                case "editpassword":
                    message = "editprofile$password" + " " + toChange;
                    dos.writeUTF(message);
                    fromServer = dis.readUTF();
                    break;
                case "editbio":
                    message = "editprofile$bio" + " " + toChange;
                    dos.writeUTF(message);
                    fromServer = dis.readUTF();
                    break;
            }
            if (isLoggedIn) {
                if (fromServer.startsWith("profile$")) {
                    this.usernameToShow.setText(user.getUsername());
                    this.bioToShow.setText(user.getBio());
                }else {
                    this.serverReactToChanges.setText(fromServer);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
