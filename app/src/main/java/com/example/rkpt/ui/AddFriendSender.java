package com.example.rkpt.ui;

import android.util.Log;
import android.widget.TextView;

import java.io.IOException;

import static com.example.rkpt.ui.Login.dis;
import static com.example.rkpt.ui.Login.dos;

public class AddFriendSender extends Thread {
    private String username;
    private TextView serverReact;
    private String fromServer;
    AddFriendSender(String username,TextView serverReact){
        this.username = username;
        this.serverReact = serverReact;
    }
    @Override
    public void run() {
        try {
            dos.writeUTF("addfriend$"+username);
            fromServer = dis.readUTF();
        } catch (IOException e) {
            e.printStackTrace();
        }
        serverReact.setText(fromServer);
    }
}
