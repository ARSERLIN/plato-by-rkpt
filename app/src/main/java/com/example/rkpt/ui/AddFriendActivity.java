package com.example.rkpt.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.rkpt.R;
import com.example.rkpt.ui.people.PeopleFragment;

public class AddFriendActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_friend);
        Button back = findViewById(R.id.back_to_friends);
        final EditText friendUsername = (EditText) findViewById(R.id.friend_name_search);
        final Button add = findViewById(R.id.add_friend_button);
        final TextView addResult = findViewById(R.id.add_friend_reaction);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String enteredFriendUsername = friendUsername.getText().toString();
                AddFriendSender addFriendSender = new AddFriendSender(enteredFriendUsername,addResult);
                addFriendSender.start();
                try {
                    addFriendSender.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        final Intent intent = new Intent(this,HomePage.class);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(intent);
            }
        });
    }

}
