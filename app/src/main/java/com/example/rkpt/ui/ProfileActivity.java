package com.example.rkpt.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.rkpt.R;
import com.example.rkpt.ui.home.HomeFragment;

public class ProfileActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_activity);
        Button back = findViewById(R.id.back_to_home);
        TextView username = findViewById(R.id.username_in_profile);
        TextView bio = findViewById(R.id.bio);
        ActivityStatusSender activityStatusSender = new ActivityStatusSender("profile");
        activityStatusSender.setUsernameToShow(username);
        activityStatusSender.setBioToShow(bio);
        activityStatusSender.start();
        try {
            activityStatusSender.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ImageButton setting = findViewById(R.id.setting_icon);
        final Intent toSetting = new Intent(this,SettingActivity.class);
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(toSetting);
            }
        });
        Button editProfile = findViewById(R.id.edit_profile);
        final Intent toEdit = new Intent(this,EditProfileActivity.class);
        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(toEdit);
            }
        });
        final Intent toHome = new Intent(this,HomePage.class);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(toHome);
            }
        });
    }
}
